export default class tradeService {
    constructor() {
        this._apiBase = 'https://api.exmo.com/v1';
        this._ticker = '/ticker/';
        this._orderBook = '/order_book/'
    }

    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error(`Error: ${res.status}`);
        }

        return await res.json();
    }

    getTrades = async () => {
        const res = await this.getResource(this._ticker);
        return res;
    }

    getOrderBook = async (pair, limit) => {
        const res = await this.getResource(`${this._orderBook}?pair=${pair}&limit=${limit}`);
        return res;
    }
}