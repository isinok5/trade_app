import React, {Component} from 'react';
import TradeService from '../services/tradeService';
import ErrorMessage from './Error';

export default class Orders extends Component {
    TradeService = new TradeService();
    state = {
        orders: {},
        err: false,
    }

    componentDidMount() {
        this.updateOrders();
    }

    updateOrders() {
        this.TradeService.getOrderBook(this.props.pair, this.props.limit)
        .then(this.onOrdersLoaded)
        .catch(this.onError);
    }

    onOrdersLoaded = (orders) => {
        this.setState ({
             orders,
             err: false
        })
    }

    onError = () => {
        this.setState({
            err: true
        })
    }

    renderItems(obj, pair, type) {
        if (type === "sell")
        {
            return obj[pair]["ask"].map(item => {
                return (
                    <tr>
                        <td>{item[0]}</td>
                        <td>{item[1]}</td>
                        <td>{item[2]}</td>
                    </tr>
                )
            })
        } else if (type === "purchase") {
            return obj[pair]["bid"].map(item => {
                return (
                    <tr>
                        <td>{item[0]}</td>
                        <td>{item[1]}</td>
                        <td>{item[2]}</td>
                    </tr>
                )
            })
        }
    }

    render() {
        const {err} = this.state;
        
        if (err) {
            return <ErrorMessage/>
        }

        if (Object.keys(this.state.orders).length)
        {
            const {pair, type} = this.props;
            const {orders} = this.state;
            const items = this.renderItems(orders, pair, type);
            return (
                    <table>
                        <thead>
                            <tr>
                                <th>Цена</th>
                                <th>Количество</th>
                                <th>Сумма</th>
                            </tr>
                        </thead>
                        <tbody>
                            {items}
                        </tbody>
                    </table>
            )
        } else {
            return (
                <div>
                </div>
            )
        }
    }
}