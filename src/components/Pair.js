import React from 'react';

const Pair = (props) => {
    const {name, buy_price, avg, high, low, vol, vol_curr, symbol} = props;
    let color='';
    if (symbol === "▲")
        color = "green";
    else if (symbol === "▼")
        color = "red";
    return (
        <div className="pair">
            <div className="top">
                <h1>{name}</h1>
                <span><span style={{color: `${color}`}}>{symbol} </span>{Math.round(buy_price * 100000) / 100000}</span>
            </div>
            <p style={{marginLeft: "3%"}}>Статистика за 24 часа:</p>
            <div className="left">
                <p>Объем торгов:   {Math.round(vol * 100000) / 100000}</p>
                <p>Сумма сделок:   {Math.round(vol_curr * 100000) / 100000}</p>
            </div>
            <div className="right">
                <p>Средняя цена сделки:   {Math.round(avg * 100000) / 100000}</p>
                <p>Максимальная цена сделки:  {Math.round(high * 100000) / 100000}</p>
                <p>Минимальная цена сделки:   {Math.round(low * 100000) / 100000}</p>
            </div>
        </div>
    )
}

export default Pair;