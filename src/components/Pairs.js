import React, {Component} from 'react';
import TradeService from '../services/tradeService';
import PopUp from './PopUp';
import ErrorMessage from './Error';

export default class Pairs extends Component {
    TradeService = new TradeService();
    state = {
        trades: {},
        newTrades: {},
        buf: {},
        showPairs: true,
        showPopUp: false,
        err: false
    }

    componentDidMount() {
        this.interval = setInterval(() => this.updateTrades(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateTrades() {
        this.TradeService.getTrades()
        .then(this.onTradesLoaded)
        .catch(this.onError)
    }

    onTradesLoaded = (trades) => {
        if (Object.keys(this.state.trades).length === 0) {
            this.setState ({
                trades,
                err: false
            })
        } else {
            this.setState (prevTrades => ({
                    trades: prevTrades.trades,
                    newTrades: trades,
                    err: false
            }))
        }
    }

    onError = () => {
        this.setState({
            err: true
        })
    }

    showPopUp = (key) => {
        clearInterval(this.interval);
        this.setState({
            showPairs: !this.state.showPairs,
            showPopUp: !this.state.showPopUp,
            buf: key,
        })
    }

    compareStateOfTrade(last, curr) {
        let res = {
            type: '',
            symbol: ''
        }
        
        if (curr > last) {
            res.type = "_up";
            res.symbol = "▲";
        }
            
        else if (curr < last)
        {
            res.type = "_down";
            res.symbol = "▼";
        }
        return res;
    }

    renderItems(obj) {
        return Object.keys(obj).map(key => {
             const {buy_price} = obj[key];
             let round_price = Math.round(buy_price * 100000) / 100000;
             let round_last_price = Math.round(this.state.trades[key].buy_price * 100000) / 100000
             let res = this.compareStateOfTrade(round_last_price, round_price);
             return (
                 <div className="pairs" key={key} onClick={() => this.showPopUp(key)}>
                     <p className={"left" + res.type}>{key} </p>
                     <p className="right"><span className={"right" + res.type}>{res.symbol} </span>{round_price}</p>
                 </div>
             )
         })
    }

    render() {
        const {trades, newTrades, err} = this.state;

        if (err) {
            return <ErrorMessage/>
        }

        const items = (Object.keys(this.state.newTrades).length === 0)
                ? this.renderItems(trades) 
                : this.renderItems(newTrades);
        return (
            <div className="container">
                {this.state.showPairs ? items : null}
                {this.state.showPopUp ? <PopUp trade={this.state.buf}/> : null}
            </div>
        )
    }
}