import React, {Component} from 'react';
import TradeService from '../services/tradeService';
import Pair from './Pair';
import Orders from './Orders';
import Pairs from './Pairs';
import ErrorMessage from './Error';

export default class PopUp extends Component {
    TradeService = new TradeService();
    state = {
        trades: {},
        lastPrice: '',
        showPopUp: true,
        showPairs: false,
        err: false,
    }

    componentDidMount() {
        this.interval = setInterval(() => this.updateTrades(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateTrades() {
        this.TradeService.getTrades()
        .then(this.onTradesLoaded)
        .catch(this.onError)
    }

    onTradesLoaded = (trades) => {
        if (Object.keys(this.state.trades).length === 0) {
            this.setState ({
                trades,
                lastPrice: trades[this.props.trade]["buy_price"],
                err: false
            })
        } else {
            this.setState (prevTrades => ({
                trades,
                lastPrice: prevTrades.trades[this.props.trade]["buy_price"],
                err: false
           }))
        }
    }

    onError = () => {
        this.setState({
            err: true
        })
    }

    closePopUp = () => {
        clearInterval(this.interval);
        this.setState({
            showPopUp: false,
            showPairs: true,
        });
    }

    render() {
        const {trade} = this.props;
        const {trades, err} = this.state;

        if (err) {
            return <ErrorMessage/>
        }

        if (Object.keys(trades).length) {
            const {buy_price, avg, high, low, vol, vol_curr} = trades[trade];
            const symbol = Math.round(buy_price * 100000) / 100000 > Math.round(this.state.lastPrice * 100000) / 100000 ? "▲" : "▼";
            console.log(trades[trade]);
            console.log(this.state.lastPrice);
            return (
                <div>
                    {this.state.showPopUp ?
                    <div className="container popup">
                        <Pair
                            name = {trade}
                            buy_price = {buy_price}
                            avg = {avg}
                            high = {high}
                            low = {low}
                            vol = {vol}
                            vol_curr = {vol_curr}
                            symbol = {symbol}
                        />
                        <button onClick={() => this.closePopUp()}></button>
                        <p style={{display: "inline-block", marginRight: "3%", fontSize: "21px", position: "relative", top: "-270px"}}>Покупка</p>
                        <Orders
                            pair={trade}
                            limit={50}
                            type={"purchase"}
                        />
                        <p style={{display: "inline-block", marginRight: "3%", fontSize: "21px", position: "relative", top: "-270px"}}>Продажа</p>
                        <Orders
                            pair={trade}
                            limit={50}
                            type={"sell"}
                        />
                    </div>
                    : null}
                    {this.state.showPairs ? <Pairs/> : null}
                </div>
            )
        } else {
            return (
                <div>
                </div>
            )
        }
        
    }
}