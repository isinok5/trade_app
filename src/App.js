import React from 'react';
import './App.css';
import Header from './components/Header';
import Pairs from './components/Pairs';
import {BrowserRouter as Router, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Header/>
        <Route path='/' component={Pairs} exact/>
      </div>
    </Router>
  );
}

export default App;
